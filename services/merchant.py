from models.merchant import merchantInDB, merchantDao


class serviceMerchant:
    @staticmethod
    def save_merchant(request: merchantInDB):
        return merchantDao().save(request)
