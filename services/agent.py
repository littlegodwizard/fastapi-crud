from models.agent import agentInDB, agentDao


class serviceAgent:
    @staticmethod
    def save_agent(request: agentInDB):
        return agentDao().save(request)
