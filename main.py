from fastapi import FastAPI
from starlette.middleware.cors import CORSMiddleware
from api.routes.api import router as api_router
from db.mongodb import connect_to_mongo


def get_application() -> FastAPI:
    application = FastAPI(title="aero-connect-backend-test")

    application.add_middleware(
        CORSMiddleware,
        allow_origins=["*"],
        allow_origin_regex='https?://.*',
        allow_credentials=True,
        allow_methods=["*"],
        allow_headers=["*"],
    )

    application.add_event_handler("startup", connect_to_mongo)

    application.include_router(api_router)

    return application


app = get_application()
