from fastapi import APIRouter

from api.routes import product, agent, merchant


router = APIRouter()

router.include_router(product.router, tags=["product"], prefix="/product")
router.include_router(agent.router, tags=["agent"], prefix="/agent")
router.include_router(merchant.router, tags=["merchant"], prefix="/merchant")
