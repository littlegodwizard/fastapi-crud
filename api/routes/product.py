from fastapi import APIRouter
from pydantic import BaseModel

from services.product import serviceProduct
from models.product import productInDB

router = APIRouter()


@router.post("/add")
async def add_merchant(request: productInDB):
    return serviceProduct().save_merchant(request)


@router.get("/all")
async def get_all_product():
    return serviceProduct().get_all_product()
