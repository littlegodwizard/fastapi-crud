from fastapi import APIRouter
from services.agent import serviceAgent
from models.agent import agentInDB

router = APIRouter()


@router.post("/add")
async def add_agent(request: agentInDB):
    return serviceAgent.save_agent(request)
